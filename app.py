########### Imports
from email import message
import email
from enum import unique
from pytz import timezone
from sqlalchemy import ForeignKey, true
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import datetime
from functools import wraps

########### App configurations
app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "secretKey"
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:@localhost/bookstore"

db = SQLAlchemy(app)

########### Db models
class users(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    name = db.Column(db.String(255))
    password = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)


class Books(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    author = db.Column(db.String(255))
    isbn = db.Column(db.String(255), unique=True)
    release_date = db.Column(db.DateTime(timezone=False))
    title = db.Column(db.String(500))
    user_id = db.Column(db.Integer)
    users_id = db.Column(db.Integer, ForeignKey("users.id"))


########## Token required function declaration
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]

        if not token:
            return jsonify({"message": "Token is missing!"}), 401
        try:
            data = jwt.decode(token, app.config["SECRET_KEY"], algorithms=["HS256"])
            current_user = users.query.filter_by(id=data["id"]).first()
        except:
            return jsonify({"message": "invalid Token!"})

        return f(current_user, *args, **kwargs)

    return decorated


############ Users register route
@app.route("/auth/register", methods=["POST"])
def create_user():

    data = request.get_json()
    try:
        hashed_password = generate_password_hash(data["password"], method="sha256")
        new_user = users(
            name=data["name"], password=hashed_password, email=data["email"]
        )
        db.session.add(new_user)
        db.session.commit()
    except:
        return jsonify({"message": "Format error"})

    return jsonify({"message": "New user created!"})


########## Login route
@app.route("/auth/login", methods=["POST"])
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response(
            "Could not verify",
            401,
            {"WWW-Authenticate": 'Basic realm="Login required!"'},
        )

    user = users.query.filter_by(email=auth.username).first()

    if not user:
        return make_response(
            "Could not verify",
            401,
            {"WWW-Authenticate": 'Basic realm="email required!"'},
        )

    if check_password_hash(user.password, auth.password):
        token = jwt.encode(
            {
                "id": user.id,
                "exp": datetime.datetime.utcnow() + datetime.timedelta(hours=24),
            },
            app.config["SECRET_KEY"],
            algorithm="HS256",
        )

        return jsonify({"token": token})

    return make_response(
        "Could not verify", 401, {"WWW-Authenticate": 'Basic realm="Login required!"'}
    )


########## Register book route
@app.route("/books", methods=["POST"])
@token_required
def add_book(current_user):
    if not current_user:
        return jsonify({"message": "Cannot perform that function!"})

    data = request.get_json()
    try:
        new_book = Books(
            isbn=data["isbn"],
            title=data["title"],
            author=data["author"],
            release_date=data["release_date"],
            users_id=current_user.id,
        )
        db.session.add(new_book)
        db.session.commit()
    except:
        return jsonify({"message": "Error"})
    last_book = Books.query.filter_by(isbn=new_book.isbn).first()
    return jsonify({"message": "New book added!, book id:" + str({last_book.id})})


########### Get all books
@app.route("/books", methods=["GET"])
def get_all_books():

    books = Books.query.all()

    output = []

    for book in books:
        book_data = {}
        user = users.query.filter_by(id=book.users_id).first()
        book_data["id"] = book.id
        book_data["title"] = book.title
        book_data["author"] = book.author
        book_data["isbn"] = book.isbn
        book_data["release_date"] = book.release_date
        book_data["registered_by"] = user.name
        output.append(book_data)

    return jsonify({"books": output})


########### Delete books route
@app.route("/books/<book_id>", methods=["DELETE"])
@token_required
def delete_todo(current_user, book_id):
    if not current_user:
        return jsonify({"message": "Cannot perform that function!"})

    book = Books.query.filter_by(id=book_id).first()

    if not book:
        return jsonify({"message": "No book found!"})

    db.session.delete(book)
    db.session.commit()

    return jsonify({"message": "Book item deleted!"})


########### Test route
@app.route("/ping")
def ping():
    return jsonify({"message": "Pong"})


if __name__ == "__main__":
    app.run(debug=True, port=4000)
